# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Вячеслав Волошин <vol_vel@mail.ru>, 2015
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-13 13:55+0300\n"
"PO-Revision-Date: 2020-02-15 16:38+0200\n"
"Last-Translator: Вячеслав Волошин <vol_vel@mail.ru>\n"
"Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Poedit 2.2.1\n"

#: add-desktop.sh:15
msgid "Item Name:"
msgstr "Имя элемента:"

#: add-desktop.sh:15
msgid "My App"
msgstr "My App"

#: add-desktop.sh:15
msgid "Item Icon:FL"
msgstr "Ярлык элемента:FL"

#: add-desktop.sh:15
msgid "Item Category:CB"
msgstr "Категория элемента:CB"

#: add-desktop.sh:15
msgid "Application|AudioVideo|Development|Education|Game|Graphics|Network|Office|Settings|System|Utility"
msgstr "Приложения|АудиоВидео|Разработка|Обучение|Игры|Графика|Сеть|Офис|Установки|Система|Утилиты"

#: add-desktop.sh:15
msgid "Item Command:"
msgstr "Команда элемента:"

#: add-desktop.sh:15
msgid "Item Location:CB"
msgstr "Расположение элемента:CB"

#: add-desktop.sh:15
msgid "1) Application Menu|2) Desktop Shortcut|3) Personal"
msgstr "1) Меню приложений|2) Ярлык рабочего стола|3) Личное"

#: add-desktop.sh:15
msgid "File Name:"
msgstr "Имя файла:"

#: add-desktop.sh:15
msgid "Launch in Terminal:CHK"
msgstr "Выполнить в терминале:CHK"

#: add-desktop.sh:15
msgid "add-desktop"
msgstr "add-desktop"
