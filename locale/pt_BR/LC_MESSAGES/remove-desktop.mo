��          \      �       �   3   �      �        
        )     6  B   Q  �  �  @   N      �     �     �     �  $   �  ]                                          1) Application Menu|2) Desktop Shortcut|3) Personal Desktop Manager Item Location:CB Menu Files Menu Manager Select the items to remove Select the location you would like to remove the desktop file from Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-27 17:32+0200
Last-Translator: marcelo cripe <marcelocripe@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 1) Menu do Aplicativo2) Atalho para a Área de Trabalho3)Pessoal Gerenciador de Área de Trabalho Localização do item:CB Arquivos de Menu Gerenciador do Menu Selecione os itens a serem removidos Selecione o local de onde pretende remover o arquivo '.desktop' (atalho) da área de trabalho 